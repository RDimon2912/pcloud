# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-07-01 00:38
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('storage', '0002_auto_20170630_0938'),
        ('trash', '0001_initial'),
        ('task', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SelectedStorageFiles',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('path', models.CharField(max_length=255)),
                ('storage', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='storage.Storage')),
            ],
        ),
        migrations.CreateModel(
            name='SelectedTrashFiles',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('path', models.CharField(max_length=255)),
            ],
        ),
        migrations.RemoveField(
            model_name='selectedfiles',
            name='task',
        ),
        migrations.AlterField(
            model_name='task',
            name='mode',
            field=models.IntegerField(choices=[(0, 'Remove'), (1, 'Move to trash / Restore')], default=1),
        ),
        migrations.DeleteModel(
            name='SelectedFiles',
        ),
        migrations.AddField(
            model_name='selectedtrashfiles',
            name='task',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='trash_files', to='task.Task'),
        ),
        migrations.AddField(
            model_name='selectedtrashfiles',
            name='trash',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='trash.Trash'),
        ),
        migrations.AddField(
            model_name='selectedstoragefiles',
            name='task',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='storage_files', to='task.Task'),
        ),
    ]
