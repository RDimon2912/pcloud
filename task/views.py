# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required

from .models import Task
from .forms import TaskForm
import os
# Create your views here.

@login_required
def index(request):
	tasks = request.user.tasks.all()
	return render(request, 'task/index.html', { 'tasks' : tasks} )

@login_required
def create(request):
	form = TaskForm(request.POST or None)
	if request.method == 'POST' and form.is_valid():
		task = form.save(commit=False)
		task.user = request.user
		task.save()
		return redirect('task:index')		
	return render(request, 'task/new.html', {'form': form})

@login_required
def update(request, pk):
	task = get_object_or_404(Task, pk=pk)
	form = TaskForm(request.POST or None, instance=task)
	if request.method == 'POST' and form.is_valid():
		form.save()
		return redirect('task:index')
	return render(request, 'task/update.html', { 'form' : form, 'task' : task })

@login_required
def delete(request, pk):
	task = get_object_or_404(Task, pk = pk)
	if request.method == 'POST':
		task.delete()
		return redirect('task:index')
	return Http404('URL is does not exist')

@login_required
def show(request, pk, path):
	task = get_object_or_404(Task, pk = pk)
	path_user_dir = os.path.join(BASE_DIR, 'junk', request.user.username)
	if not os.path.exists(path_user_dir):
		os.makedirs(path_user_dir)
	path_in_task = os.path.join(path_user_dir, path)
	print path_in_task
	if not os.path.exists(path_in_task):
		raise Http404("Path does not exist")
	return redirect('task:index')

@login_required
def select(request, pk):
	task = get_object_or_404(Task, pk = pk)
	if request.method == 'POST':
		task.select()
		return redirect('task:index')
	return Http404('URL is does not exist')
