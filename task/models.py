from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

from storage.models import Storage
from trash.models import Trash

# Create your models here.

class Task(models.Model):
	STATUS_NEW = 0
	STATUS_WAITING = 1
	STATUS_PROCESSING = 2
	STATUS_ACCEPTED = 3
	STATUS_ERROR = 4
	STATUSES = (
			(STATUS_NEW, 'New'),
			(STATUS_WAITING, 'Waiting'),
			(STATUS_PROCESSING, 'Processing'),
			(STATUS_ACCEPTED, 'Accepted'),
			(STATUS_ERROR, 'Error'),
		)
	name = models.CharField(max_length=255)
	status = models.IntegerField(choices=STATUSES, default=STATUS_NEW)
	is_selected = models.BooleanField(default=False)

	user = models.ForeignKey(User, related_name='tasks')
	
	MODE_REMOVE = 0
	MODE_TRASH = 1
	MODES = (
			(MODE_REMOVE, 'Remove'),
			(MODE_TRASH, 'Move to trash / Restore'),
		)
	mode = models.IntegerField(choices=MODES, default=MODE_TRASH)

	def select(self):
		if not self.status == self.STATUS_NEW:
			return
		active = Task.objects.filter(is_selected=True)
		if active:
			active.update(is_selected=False)
		self.is_selected = True
		self.save()

	def status_name(self):
		return self.STATUSES[self.status][1]

	def mode_name(self):
		return self.MODES[self.mode][1]

class SelectedStorageFiles(models.Model):
	path = models.CharField(max_length=255)
	task = models.ForeignKey(Task,
		on_delete=models.CASCADE,
		related_name="storage_files",
	)
	storage = models.ForeignKey(Storage, 
		on_delete=models.CASCADE)

class SelectedTrashFiles(models.Model):
	path = models.CharField(max_length=255)
	task = models.ForeignKey(Task,
		on_delete=models.CASCADE,
		related_name="trash_files"
	)
	trash = models.ForeignKey(Trash,
		on_delete=models.CASCADE)