from django.conf.urls import url
from . import views

app_name = 'task'

urlpatterns = [
		
	url(r'^$', views.index, name='index'),
	url(r'^new$', views.create, name='create'),
	url(r'^update/(?P<pk>\d+)$', views.update, name='update'),
	url(r'^delete/(?P<pk>\d+)$', views.delete, name='delete'),
	url(r'^select/(?P<pk>\d+)$', views.select, name='select'),
]