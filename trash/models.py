from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import User
from utils.models.item import Item

import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Create your models here.
class Trash(models.Model):
	user = models.ForeignKey(User, related_name='trashes')
	path = models.CharField(max_length=256, default='')

	@classmethod
	def create(cls, user):
		trash = cls(user = user)
		trash.save()
		path = os.path.join(BASE_DIR, '.TRASH', user.username, trash.pk.__str__())
		trash.path = path
		trash.save()
		if not os.path.exists(path):
			os.makedirs(path)
		return trash

	def destroy_trash(self):
		item = Item.make_item_from_path(self.path)
		item.remove()
		self.delete()