from django.conf.urls import url
from trash import views

app_name = 'trash'

urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^show/(?P<pk>\d+)$', views.show, name='show'),
]
