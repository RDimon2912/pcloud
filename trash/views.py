# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required

from utils.models.item import Item

from .models import Trash
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Create your views here.

@login_required
def index(request):
	trashes = request.user.trashes.all()
	return render(request, 'trash/index.html', { 'trashes' : trashes } )

@login_required
def show(request, pk):
	trash = get_object_or_404(Trash, pk = pk)
	try:
		files = Item.make_item_from_path(trash.path).listdir()
	except Exception as e:
		return Http404('URL is does not exist')
	return render(request, 'storage/show.html', { 'files' : files })