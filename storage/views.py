# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required

from utils.models.item import Item

from .models import Storage
from .forms import StorageForm
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Create your views here.

@login_required
def index(request):
	storages = request.user.storages.all()
	return render(request, 'storage/index.html', { 'storages' : storages} )

@login_required
def create(request):
	form = StorageForm(request.POST or None)
	if request.method == 'POST' and form.is_valid():
		storage = form.save(commit=False)
		storage.user = request.user
		storage.init_storage()
		return redirect('storage:index')		
	return render(request, 'storage/new.html', {'form': form})

@login_required
def update(request, pk):
	storage = get_object_or_404(Storage, pk=pk)
	form = StorageForm(request.POST or None, instance=storage)
	if request.method == 'POST' and form.is_valid():
		form.save()
		return redirect('storage:index')
	return render(request, 'storage/update.html', { 'form' : form, 'storage' : storage })

@login_required
def delete(request, pk):
	storage = get_object_or_404(Storage, pk = pk)
	if request.method == 'POST':
		storage.destroy_storage()
		return redirect('storage:index')
	return Http404('URL is does not exist')

@login_required
def show(request, pk, path):
	storage = get_object_or_404(Storage, pk = pk)
	def get_next_path(item):
		return (item, os.path.join(path, item.name))
	try:
		items = Item.make_item_from_path(
			os.path.join(storage.path_user_dir, path)).listdir()
		files = ( get_next_path(item) for item in items )
	except Exception as e:
		return Http404('URL is does not exist')
	return render(request, 'storage/show.html', { 'pk' : pk, 'files' : files })