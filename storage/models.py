# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from utils.models.item import Item
from django.utils import timezone

from trash.models import Trash

import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Create your models here.
class Storage(models.Model):
	name = models.CharField(max_length=128)
	user = models.ForeignKey(User, related_name='storages')
	trash = models.OneToOneField(Trash, on_delete=models.CASCADE)
	path_user_dir = models.CharField(max_length=248, default='')


	def init_storage(self):
		trash = Trash.create(self.user)
		self.trash = trash
		self.save()
		self.path_user_dir = os.path.join(BASE_DIR, 'junk', self.user.username, self.pk.__str__())
		if not os.path.exists(self.path_user_dir):
			os.makedirs(self.path_user_dir)
		self.save()

	def destroy_storage(self):
		item = Item.make_item_from_path(self.path_user_dir)
		item.remove()
		self.trash.destroy_trash()
		self.delete()
	