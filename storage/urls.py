from django.conf.urls import url
from storage import views

app_name = 'storage'

urlpatterns = [
	
	url(r'^$', views.index, name='index'),
	url(r'^new$', views.create, name='create'),
	url(r'^update/(?P<pk>\d+)$', views.update, name='update'),
	url(r'^delete/(?P<pk>\d+)$', views.delete, name='delete'),
	url(r'^(?P<pk>\d+)/(?P<path>.*)$', views.show, name='show'),

]