from django import forms
from .models import Storage

class StorageForm(forms.ModelForm):
	class Meta:
		model=Storage
		exclude=["user", "path_user_dir", "trash_path", "trash"]