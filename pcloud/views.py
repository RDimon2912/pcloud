# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, get_object_or_404
import os

def home(request):
	data = []
	home_dir = os.path.expanduser('~/')
	names = os.listdir(home_dir)
	for name in names:
		new_path = os.path.join(home_dir, name)
		print new_path
		if os.path.isdir(new_path):
			is_dir = True
		else:
			is_dir = False
		data.append({'is_dir': is_dir, 'path': new_path, 'name': name})
	print data
	return render(request, 'home.html', {'files': data, 'current_dir': home}) 

def get_dir_content(request):
	data = []
	if request.method == 'GET':
		dest = request.GET['dest']
		try:
			names = os.listdir(os.path.expanduser(dest))
			for name in names:
				new_path = os.path.join(dest, name)
				if os.path.isdir(new_path):
					is_dir = True
				else:
					is_dir = False
				data.append({'is_dir': is_dir, 'path': new_path, 'name': name})
		except:
			pass
			# return render(request, 'storage/cannot_get_content.html')
	return render(request, 'home.html', {'files': data, 'current_dir': dest})
